@extends('layouts.master')

@section('judul')
Lokasi Penitipan
@endsection

@section('content')
<div>
    <h2>Edit Lokasi Penitipan</h2>
            <form action="/lokasi/{{$lokasi->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="kota">Kota</label>
                <input type="text" class="form-control" name="kota" id="kota" value="{{$lokasi->kota}}" placeholder="Masukkan Kota">
                @error('kota')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="kode_pos">Kode Pos</label>
                <input type="text" class="form-control" name="kode_pos" id="kode_pos" value="{{$lokasi->kode_pos}}" placeholder="Masukkan Kode Pos">
                @error('kode_pos')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="detail_lokasi">Detail Lokasi</label>
                <input type="text" class="form-control" name="detail_lokasi" id="detail_lokasi" value="{{$lokasi->detail_lokasi}}" placeholder="Masukkan Detail Lokasi">
                @error('detail_lokasi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
</div>
@endsection