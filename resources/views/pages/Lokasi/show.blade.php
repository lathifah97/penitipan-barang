@extends('layouts.master')

@section('judul')
Lokasi Penitipan
@endsection

@section('content')

<h2>Lokasi Penitipan {{$lokasi->id}}</h2>
<h4>{{$lokasi->kota}}</h4>
<h4>{{$lokasi->kode_pos}}</h4>
<p>{{$lokasi->detail_lokasi}}</p>

@endsection