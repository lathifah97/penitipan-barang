@extends('layouts.master')

@section('judul')
Lokasi Penitipan
@endsection

@section('content')
<div>
    <h2>Pilih Lokasi Penitipan</h2>
        <form action="/lokasi" method="POST">
            @csrf
            <input type="text" class="form-control" name="users_id" id="kota" placeholder="Masukkan kota" hidden value="{{ Auth::user()->id }}">
            <div class="form-group">
                <label for="kota">Kota</label>
                <input type="text" class="form-control" name="kota" id="kota" placeholder="Masukkan kota">
                @error('kota')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="kode_pos">Kode Pos</label>
                <input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Masukkan Kode Pos">
                @error('kode_pos')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="detail_lokasi">Detail Lokasi</label>
                <input type="text" class="form-control" name="detail_lokasi" id="detail_lokasi" placeholder="Masukkan Detail Lokasi">
                @error('detail_lokasi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection