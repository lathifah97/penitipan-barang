@extends('layouts.master')

@section('judul')
Profile
@endsection

@section('content')

<h2>Profile <b>{{ Auth::user()->email }}</b></h2>

@if($Profile=='NO DATA')
    <a href="profiles/create" class="btn btn-primary">EDIT</a>
    <h4>{{$Profile}}</h4>
@else
    <a href="profiles/{{$Profile[0]->id}}/edit" class="btn btn-primary">EDIT</a>
    <form action="/profiles/{{$Profile[0]->id}}" method="POST">
        @csrf
        @method('DELETE')
        <input type="submit" class="btn btn-danger my-1" value="Delete">
    </form>
    <br>
    <label for="nama">Nama</label>
    <h4>{{$Profile[0]->nama}}</h4>
    <label for="no_hp">HP</label>
    <h4>{{$Profile[0]->no_hp}}</h4>
    <label for="alamat">Alamat</label>
    <h4>{{$Profile[0]->alamat}}</h4>
@endif
@endsection