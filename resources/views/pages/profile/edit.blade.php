@extends('layouts.master')

@section('judul')
Profile
@endsection

@section('content')
<div>
    <h2>Profile</h2>
        <form action="/profiles/{{$Profile[0]->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama" value="{{$Profile[0]->nama}}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <input type="text" class="form-control" name="users_id" value="{{$Profile[0]->users_id}}" hidden>
            <div class="form-group">
                <label for="no_hp">HP</label>
                <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Masukkan HP" value="{{$Profile[0]->no_hp}}">
                @error('no_hp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat" value="{{$Profile[0]->alamat}}">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
</div>
@endsection