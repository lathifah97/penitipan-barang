@extends('layouts.master')

@section('judul')
Profile
@endsection

@section('content')
<div>
    <h2>Profile</h2>
        <form action="/profiles" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <input type="text" class="form-control" name="users_id" value="{{ Auth::user()->id }}" hidden>
            <div class="form-group">
                <label for="no_hp">HP</label>
                <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="Masukkan HP">
                @error('no_hp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection