<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Lokasi;
use Illuminate\Support\Facades\Auth;

class LokasiController extends Controller
{
    public function index()
    {
        $Lokasi=Lokasi::where('users_id', Auth::user()->id)->get();
        return view('pages.lokasi.index', compact('Lokasi'));
    }
    public function create()
    {
        return view('pages.lokasi.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'kota' => 'required',
            'kode_pos' => 'required',
            'detail_lokasi' => 'required',
            'users_id' => 'required'
        ]);
        // dd($request);
        Lokasi::create($request->all());
        return redirect('/lokasi');
    }
    public function show($id)
    {
        $lokasi = DB::table('lokasi_penitipans')->where('id', $id)->first();
        return view('pages.lokasi.show', compact('lokasi'));
    }
    public function edit($id)
    {
        $lokasi = DB::table('lokasi_penitipans')->where('id', $id)->first();
        return view('pages.lokasi.edit', compact('lokasi'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'kota' => 'required',
            'kode_pos' => 'required',
            'detail_lokasi' => 'required'
        ]);
        $query = DB::table('lokasi_penitipans')
            ->where('id', $id)
            ->update([
                'kota' => $request["kota"],
                'kode_pos' => $request["kode_pos"],
                'detail_lokasi' => $request["detail_lokasi"]
            ]);
        return redirect('/lokasi');
    }

    public function destroy($id)
    {
        $query = DB::table('lokasi_penitipans')->where('id', $id)->delete();
        return redirect('/lokasi');
    }
}
