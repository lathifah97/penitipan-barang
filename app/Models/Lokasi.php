<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    use HasFactory;
    protected $table = "lokasi_penitipans";
    protected $fillable = ["kota", "kode_pos", "detail_lokasi","users_id"];
}
