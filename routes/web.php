<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LokasiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
	Route::get('/', function () {
	return view('home');
	});
	Route::resource('profiles', ProfileController::class);
	Route::resource('lokasi', LokasiController::class);

});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
